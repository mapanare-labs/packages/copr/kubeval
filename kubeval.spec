%define debug_package %{nil}

Name:           kubeval
Version:        0.16.1
Release:        0%{?dist}
Summary:        Validate your Kubernetes configuration files, supports multiple Kubernetes versions

License:        ASL 2.0
URL:            https://kubeval.com/
Source0:        https://github.com/instrumenta/%{name}/releases/download/v%{version}/%{name}-linux-amd64.tar.gz

%description
Validate your Kubernetes configuration files, supports multiple Kubernetes versions

%prep
%autosetup -n %{name} -c

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/usr/bin
install -p -m 755 %{name} %{buildroot}/usr/bin/kubeval

%files
%license LICENSE
%doc README.md
/usr/bin/kubeval

%changelog
* Thu Sep 15 2022 Enmanuel Moreira <enmanuelmoreira@gmail.com>
- Initial RPM